#!/usr/bin/python3
import os
import sys
import yaml
import argparse
import gitlab
import re

# Gather the command line arguments we need
parser = argparse.ArgumentParser(description='Verifies the metadata files')
parser.add_argument('--metadata-path', help='Path to the metadata to check', required=True)
args = parser.parse_args()

# Make sure our configuration file exists
if not os.path.exists( args.metadata_path ):
    print("Unable to locate specified metadata location: %s".format(args.metadata_path))
    sys.exit(1)

# Connect to invent.kde.org instance
gl = gitlab.Gitlab("https://invent.kde.org")

# Regular expresssion to match if name includes anything other than supported
reg = re.compile('^[a-zA-Z0-9_]([a-z]|[A-Z]|[0-9]|_|-| |\.*)')

# Start going over the location in question...
for currentPath, subdirectories, filesInFolder in os.walk( args.metadata_path, topdown=False, followlinks=False ):
    # Do we have a metadata.yaml file?
    if 'metadata.yaml' not in filesInFolder:
        # We're not interested then....
        continue

    # Now that we know we have something to work with....
    # Lets load the current metadata up
    metadataPath = os.path.join( currentPath, 'metadata.yaml' )
    metadataFile = open( metadataPath, 'r' )
    metadata = yaml.load( metadataFile )

    # check sanity of the description and make sure that it is less then 250
    # (part of gitlab restriction)
    if metadata['description'] is not None and len(metadata['description']) > 250:
        print("Invalid size of description in the " + currentPath)

    # check the description and ensure that it is not empty
    # currently warning, but still best to list it out
    if metadata['description'] is not None and len(metadata['description']) == 0:
        print("Empty description in the " + currentPath)

    # Check if name have supported characters only
    if not reg.match(metadata['name']):
        print("Metadata contains the invalid characters in name " + currentPath)
        print(metadata['name'])

    # Make sure that identifier is not empty
    identifier = metadata.get('identifier')
    if identifier is None:
        print("Fatal error: Empty identifier in the " + currentPath)
        sys.exit(1)

    # create gitlab project instance
    gl_project = gl.projects.get(metadata['repopath'])

    # Make sure that if gitlab repo have description it is not different then what is in repo-metadata
    # if description is not empty, add it in metadata.
    if gl_project.description is not None and len(gl_project.description) > 0:
        if gl_project.description != metadata['description']:
            print("Description on repo-metadata and invent does not match in " + currentPath)

# All done!
sys.exit(0)


